package com.inteligr8.github.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.github.http.BaseResponse;

public class CreatePullRequest {
	
	private CreatePullRequest() {
	}
	
	public static String constructRequestPath(String repoName) {
		return "/repos/" + repoName + "/" + httpPath;
	}
	
	public static String httpPath = "pulls";
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Request {
		
		@JsonProperty(required = true)
		private String title;
		@JsonProperty(required = true)
		private String head;
		@JsonProperty(required = true)
		private String base;
		private String body;
		@JsonProperty(value = "maintainer_can_modify")
		private Boolean maintainerCanModify;
		private Boolean draft;
		
		public String getTitle() {
			return title;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}
		
		public String getHead() {
			return head;
		}
		
		public void setHead(String head) {
			this.head = head;
		}
		
		public String getBase() {
			return base;
		}
		
		public void setBase(String base) {
			this.base = base;
		}
		
		public String getBody() {
			return body;
		}
		
		public void setBody(String body) {
			this.body = body;
		}
		
		public Boolean getMaintainerCanModify() {
			return maintainerCanModify;
		}
		
		public void setMaintainerCanModify(Boolean maintainerCanModify) {
			this.maintainerCanModify = maintainerCanModify;
		}
		
		public Boolean getDraft() {
			return draft;
		}
		
		public void setDraft(Boolean draft) {
			this.draft = draft;
		}
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Response extends BaseResponse {
		
		private String url;
		private int id;
		private String title;
		
		public String getUrl() {
			return this.url;
		}
		
		public void setUrl(String url) {
			this.url = url;
		}
		
		public int getId() {
			return this.id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public String getTitle() {
			return this.title;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}

	}
	
}
