package com.inteligr8.github.model;

public class DeleteReference {
	
	private DeleteReference() {
	}
	
	public static String constructRequestPath(String repoName, String ref) {
		return "/repos/" + repoName + "/" + httpPath + "/" + ref;
	}
	
	public static String constructRequestPathByBranch(String repoName, String branchName) {
		return constructRequestPath(repoName, "refs/heads/" + branchName);
	}
	
	public static String httpPath = "git";

}
