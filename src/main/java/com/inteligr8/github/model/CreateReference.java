package com.inteligr8.github.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.github.http.BaseResponse;

public class CreateReference {
	
	private CreateReference() {
	}
	
	public static String constructRequestPath(String repoName) {
		return "/repos/" + repoName + "/" + httpPath;
	}
	
	public static String httpPath = "git/refs";
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Request {
		
		@JsonProperty(required = true)
		private String ref;
		@JsonProperty(required = true)
		private String sha;
		
		public String getRef() {
			return this.ref;
		}
		
		public void setRef(String ref) {
			this.ref = ref;
		}
		
		public String getSha() {
			return this.sha;
		}
		
		public void setSha(String sha) {
			this.sha = sha;
		}
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Response extends BaseResponse {
		
	}

}
