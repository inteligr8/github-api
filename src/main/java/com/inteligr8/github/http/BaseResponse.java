package com.inteligr8.github.http;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
	
	private int httpStatusCode;
	private String httpStatusReason;
	
	public int getHttpStatusCode() {
		return this.httpStatusCode;
	}
	
	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
	public String getHttpStatusReason() {
		return this.httpStatusReason;
	}
	
	public void setHttpStatusReason(String httpStatusReason) {
		this.httpStatusReason = httpStatusReason;
	}

}
